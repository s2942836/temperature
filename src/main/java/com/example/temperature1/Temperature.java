package com.example.temperature1;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Temperature extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Converter quoter;

    public void init() throws ServletException {
        quoter = new Converter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celsius to F";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius : " +
                request.getParameter("isbn") + "\n" +
                "  <P>Fahrenheit : " +
                Double.toString(quoter.converter(request.getParameter("isbn"))) +
                "</BODY></HTML>");
    }


}
